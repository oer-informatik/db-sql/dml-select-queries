## Funktionen im SELECT-Clause (SingleRowFunctions)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/sql-dml-singlerowfunctions</span>



> **tl/dr;** _(ca. 5 min Lesezeit): In der Projektion eines `SELECT`-Statements können Funktionen genutzt werden, die skalar oder aggregierend sind, die Zahlen, Zeichenketten, Datumswerte oder auch die Ablaufsteuerung betreffen. Dieser Artikel gibt eine kurze Übersicht._


### Unterschiedliche Arten von Funktionen in SQL-Statements:

Unterscheidung 1: Liefern die Funktionen bei mehrmaligem Ausführen immer das selbe Ergebnis?

|deterministisch | nicht determinisisch |
| -------- | -------- |
| gleicher Eingabewert = gleiches Ergebnis     | gleiche Parameter = ggf. unterschiedliche Ergebnisse |
| Beispiel: Summe     | aktuelle Zeit     |

(Ein Unterpunkt wäre hier noch die Idempotenz: `zahl=zahl+1` ist zwar deterministisch, aber liefert bei mehrmaliger Ausführung andere Ergebnisse als bei einmaliger Ausführung)


Unterscheidung 2: Beziehen sich die Funktionen auf unterschiedliche Datensätze?

|Aggregierende Funktionen| Skalare Funktionen |
| -------- | -------- |
|Sogenannte _multi row functions_  werten Attributwerte spaltenweise aus.<br/>Details dazu in [diesem Artikel zu `GROUP`-Functions](https://oer-informatik.de/sql-groupfunctions) | Das Gegenteil sind _single row functions_, <br/>die lediglich Attributwerte innerhalb eines Datensatzes auswerten. |
|`max(bruttokosten)`<br/>Gibt das Maximum aller Werte dieser Spalte aus     | `(anzahl*(preis-rabatt))*mwst` <br/>  Berechnet für einen Datensatz (eine Zeile) einen Wert|

#### Skalare Funktionen

Einteilung der Funktionen:

- Numerische Funktionen

- Zeichenkettenfunktionen

- Datums- und Zeitfunktionen

- Ablaufsteuerungsfunktionen

#### Numerische Funktionen

| Datentyp| MSSQL |MySQL | OracleDB | postgreSQL | SQLite|
| -------- | -------- | -------- |-------- |-------- |-------- |
| Runden | `ROUND(numeric_exp, integer_exp)` |`ROUND`| | |`ROUND(Zahl, (Stellen))` |
| Gleitkommazahl formatiert ausgeben | `FORMAT(value, format)` |`FORMAT`| | |?|
| Datum/Zeit formatiert ausgeben | `FORMAT(value, format [, culture])` |`DATE_FORMAT`| | |`strftime('%Y-%m-%d','datum')` |
| Dokumentation / Übersicht Berechnungsfunktionen (Link)| [MS Doc zu numerischen Funktionen](https://docs.microsoft.com/de-de/sql/odbc/reference/appendixes/numeric-functions?view=sql-server-ver15) [MS Doc zu Formatierungsfunktionen](https://docs.microsoft.com/de-de/sql/t-sql/functions/format-transact-sql), [T-SQL Doc](https://docs.microsoft.com/de-de/sql/t-sql/functions/round-transact-sql) |[MySQL Doc zu numerischen Funktionen](https://dev.mysql.com/doc/refman/8.0/en/numeric-functions.html)| | |[Daumsfunktionen](https://www.sqlite.org/lang_datefunc.html), [Haupfunktionen](https://www.sqlite.org/lang_corefunc.html) |

#### Zeichenkettenfunktionen

| Datentyp| MSSQL |MySQL | OracleDB | postgreSQL | SQLite|
| -------- | -------- | -------- |-------- |-------- |-------- |
| Zeichenkettenlänge<br/>Anzahl der Zeichen | `LEN (string_exp)`  |`CHAR_LENGTH(str)`| |`char_length ( text )`|`LENGTH`|
| Zeichenkettenlänge<br/> in Byte| `DATALENGTH (string_exp)`  |`LENGTH(str)`| |`bit_length ( text )`||
| Substring rechts | `RIGHT(RTRIM(str), pos)`  |`RIGHT(str,len)`| | |`SUBSTR(X, Y)`|
| Substring links | `LEFT (str, pos)`  |`LEFT(str,len)`| | `substring ( string text [ FROM start integer ] [ FOR count integer ] )`|`SUBSTR(X, -Y)`|
| Substring mitte | `SUBSTRING (expression, start, length)` |`SUBSTR(str, pos [, len])`| | |`SUBSTR(X, Y, Z)`|
| Position eines Substring finden | `PATINDEX ('%pattern%', expression)`  |`INSTR(str,substr)`<br />`LOCATE(substr,str [,pos])`| | `position ( substring text IN string text )` |`INSTR`|
| Substring in Ausgabe ersetzen | `REPLACE (string_exp, string_pattern, string_replacement)`  |`REPLACE(str, from_str, to_str)`| |  |`REPLACE`|
| Zeichenketten zusammenführen | `CONCAT (string_value1, string_value2, ...)`   |`CONCAT(str1,str2,...)`| | | |
| Whitespaces entfernen | `TRIM (string_exp)`<br/>`LTRIM (string_exp)`<br/>`RTRIM (string_exp)` |`TRIM([{BOTH \| LEADING \| TRAILING} [remstr] FROM] str)`<br/>`LTRIM(str)`<br/> `RTRIM(str)`| | |`LTRIM`, `RTRIM`, `TRIM`|
| Mit RegEx suchen | `WHERE name REGEXP "^H.*-.*"` | ` ` | | `WHERE regexp_like( text, pattern [, flags])`||
| Mit RegEx ersetzen | ``|``| |`regexp_replace ( text, pattern, replacement) `| |
| Dokumentation / Übersicht Stringfunktionen (Link)| [MS Doc](https://docs.microsoft.com/de-de/sql/t-sql/functions/string-functions-transact-sql?view=sql-server-ver15) | [MySQL Doc](https://dev.mysql.com/doc/refman/8.0/en/string-functions.html)| |[PostGreSQL-Doc](https://www.postgresql.org/docs/current/functions-string.html) |[SQLite Doc](https://www.sqlite.org/lang_corefunc.html)|

Beispielabfragen zur Zeichenmanipulation:

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MariaDB/MySQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQL Server</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="oracle" onclick="openTabsByDataAttr('oracle', 'sql')">Oracle</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="sqlite" onclick="openTabsByDataAttr('sqlite', 'sql')">SQLite</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

```sql
SELECT * FROM db.table WHERE text LIKE '%MySQL%'
DELETE FROM tbl
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

```sql
SELECT LEFT('Max Mustermann', 8) AS Beispieltext
```

Ebenso kann ein Substring von rechts gelesen werden:

```sql
SELECT RIGHT('Max Mustermann', 8) AS Beispieltext
```

Bei `CHAR`-Attributen scheint SQL-Server jedoch auch die Füllzeichen mitzuzählen, weshalb die Ergebnissmenge leer sein kann. Daher sollte in diesem Fall immer ein `RTRIM` voran geschickt werden, um Whitespaces zu entfernen.

```sql
SELECT RIGHT(RTRIM(name), 8) AS Beispieltext
```

Die Zeichenkettenlänge lässt sich mit `LEN` bestimmen, wobei Whitespaces auf der linken Seite mitgezählt werden, auf der rechten Seite jedoch nicht:

```sql
SELECT LEN('Mustermann, Max') AS Beispieltext
```

ergibt: `15` 

```sql
SELECT LEN('Mustermann, Max ') AS Beispieltext
```

ergibt: `15` 

```sql
SELECT LEN(' Mustermann, Max ') AS Beispieltext
```

ergibt: `16` 

Position eines Zeichens / Zeichenmusters in einer Zeichenkette wird mit `PATINDEX` bestimmt. Es können relativ weitreichende Muster gesucht werden. Die einfachsten Wildcards sind `%` für beliebiges Zeichen davor oder danach, wie bei `LIKE`. Um die Position eines Kommas mit Leerzeichen zu finden wäre die Eingabe also:

```sql
SELECT PATINDEX('%, %', 'Mustermann, Max') AS Kommaposition 
```

Wenn das Prozent selbst gesucht wird, muss es in eckigen Klammern _escaped_ werden.

```sql
SELECT PATINDEX('% [%]%', 'Es gibt 100% nichts zu tun.') AS Prozentposition 
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

```sql
SELECT * FROM db.table WHERE text LIKE '%PostgreSQL%'
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="oracle" style="display:none">

```sql
SELECT * FROM db.table WHERE text LIKE '%Oracle%'
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="sqlite" style="display:none">

```sql
SELECT * FROM db.table WHERE text LIKE '%SQLite%'
```

</span>

#### Datums- und Zeitfunktionen

| Datentyp| MSSQL |MySQL | OracleDB | postgreSQL | SQLite|
| -------- | -------- | -------- |-------- |-------- |-------- |
| das aktuelle Datum | `CURRENT_DATE()`, `GETDATE()`, `CONVERT(date, SYSDATETIME())` |`CURDATE`| | |`date('now')`|
| die aktuelle Zeit | `CURRENT_TIME[(Zeit Genauigkeit)]`, `CONVERT(time, SYSDATETIME())` | `CURTIME`| | |`time('now','localtime')`|
| Zeitdifferenz zweier Zeitangaben in Stunden | `DATEDIFF(hour, start, end)` |`TIMESTAMPDIFF(HOUR, start, end)`| | |`SELECT CAST ((julianday(end) - julianday(start)) AS INTEGER)` |
| Zeitdifferenz zweier Datumsangaben in Tagen | `DATEDIFF(day, start, end)` |`TIMESTAMPDIFF(DAY, start, end)`| | |`SELECT CAST ((julianday(end) - julianday(start)) * 24 AS INTEGER)`|
| Datum/Zeit formatiert ausgeben | `CAST(exp AS data_type[(length)])`, `FORMAT`|`DATE_FORMAT`, `TIME_FORMAT`| | |`SELECT strftime('%Y-%m-%d','datum')`|
|Dokumentation / Übersicht Datumsfunktionen(Link)|https://docs.microsoft.com/de-de/sql/odbc/reference/appendixes/time-date-and-interval-functions?view=sql-server-ver15 |https://dev.mysql.com/doc/refman/8.0/en/date-and-time-functions.html| | |https://www.sqlite.org/lang_datefunc.html|


