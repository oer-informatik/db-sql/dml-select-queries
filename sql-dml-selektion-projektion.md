# Einfache Abfragen mit Selektion und Projektion (SQL-DML/DQL)

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/sql-dml-selektion-projektion</span>

> **tl/dr;** _(ca. 5 min Lesezeit): Der am häufigsten benötigte SQL-Befehl ist der `SELECT`-Befehl. In diesem Grundlagenartikel werden die Bestandteile Projektion und Selektion sowie die einzelnen Clauses einfacher `SELECT`s beschrieben._


Wie die anderen CRUD-Befehle (*C*reate, *R*ead, *U*pdate, *D*elete) ist er Bestandteil der Data Manipulation Language (DML). Er repräsentiert die *R*ead-Funktionalität - also die lesende Auswahlabfrage. Wegen der herausragenden Rolle erhält dieser Sprachbestandteil in der Literatur gelegentlich auch einen eigenen Namen: Data Query Language (DQL).

Der grundsätzliche Aufbau einer Auswahlabfrage ist folgender:

```sql
SELECT ProjektionDerSpalten FROM Quelle WHERE BedingungDerZeilen
```

Im vereinfachten Railroad-Diagramm wird die kommagetrennte Struktur der Projektion deutlich:

![](image/select-railroad-projektion-selektion.png)

Die Daten einer Quelle werden hierbei auf zweierlei Arten gefiltert:

## Projektion:  Spaltenauswahl

Der `SELECT`-Clause projiziert die Spalten der Quelle auf das gewünschte Ergebnis.
Er schränkt das Ergebnis der Abfrage horizontal ein.

Beispiele:

- `SELECT *`
  wählt alle Spalten der Quelle aus
  (der Asterisk \* dient als Wildcard: alle Spalten)

- `SELECT Vorname, Nachname`:
  wählt einzelne Spalten aus; nur die genannten Spalten werden angezeigt

- `SELECT Vorname AS SurName, Nachname AS LastName`
  wählt einzelne Spalten aus und ändert deren Namen (Alias)

- `SELECT preis, anzahl, preis*anzahl AS kosten`
  wählt einzelne Spalten aus und berechnet neue Werte (Operatoren +, -, *, / )

## Selektion:  Zeilenauswahl

![Railroad-Diagramm des Where-Clause](image/where-railroad.png)

Der `WHERE`-Clause begrenzt die angezeigten Datensätze / Zeilen. Er ist optional.
Er schränkt das Ergebnis der Abfrage vertikal ein.

Nur die Datensätze, die der Bedingung entsprechen werden angezeigt. Die Bedingung kann aus mehreren Teilbedingungen zusammengesetzt sein. Jede Bedingung ist ein boolescher Ausdruck, muss also als `true` oder `false` auswertbar sein.

Der WHERE-Clause-Bestandteil, der oben mit `SelektionDerZeilen` bezeichnet wurde, ist ein einfacher Boolescher Ausdruck. Dieser kann natürlich aus mehreren Booleschen Ausdrücken bestehen rekursiv aufgebaut werden:

![](image/BoolescherAusdruck.png)

Beispiele für Bedingungen sind:

### Boolesche Bedingungen

- `WHERE true` / `WHERE false`
Jede Bedingung muss als Boolescher Wert auswertbar sein - daher sind die einfachsten und kürzesten Bedingungen `true` und `false`. Zum Einsatz kommen Sie natürlich bestenfalls als Platzhalter.

- logische Operatoren
Mit den logischen Operatoren `AND`, `OR`, `NOT` können einfache Bedingungen miteinander verknüpft werden. Die Klammerung hilft oft dabei, Fehler zu vermeiden! (Anderfalls gilt: `AND` vor `OR`)

### Numerische Bedingungen

-	`WHERE preis>2.00`
   einfache numerische Bedingung

- `WHERE (((preis>2.00) AND (preis<10)) OR (NOT (anzahl =0)))`

- numerische Operatoren für Bedingungen:

|gleich|	kleiner|	kleinergleich	| größer	| größergleich	| ungleich|
| -------- | -------- | -------- |-------- |-------- |-------- |
|`=` |	`<`|	`<=`| `>`|	`>=`|	`<>`|

- `WHERE (preis*anzahl>2.00)`
   Berechnungen können Teil der Bedingung sein

- `WHERE (2.00> preis*anzahl)`
   Die Reihenfolge spielt bei Bedingungen keine Rolle.

- Bedingungen mit `BETWEEN` zur Bereichsauswahl:
      `WHERE (Price BETWEEN 10 AND 22)`
      beide Grenzen sind eingeschlossen. `BETWEEN` ist nur eine Kurzschreibweise für zwei Vergleiche, z.B. für
      `WHERE (Price >= 10 AND Price <= 22)`

### Bedingungen für Zeichen und Zeichenketten

- `WHERE (Vorname = 'Hannes')`
   exakte Übereinstimmung von Zeichenketten kann ebenfalls mit Hilfe des Gleichheits-Operators erfolgen.
   Groß- und Kleinschreibung wird hierbei von den DBMS i.d.R. nicht beachtet (kann aber per Konfiguration aktiviert werden - ausprobieren!)

- Bedingungen mit Wildcards:

   - `%` steht für mehrere Zeichen:

      - `WHERE (Vorname LIKE '%bert')` findet Hubert und Robert
      - `WHERE (Vorname LIKE 'Wolf%')` findet Wolfgang und Wolfram
      - `WHERE (Vorname LIKE '%ela%')`  findet Daniela, Melanie, Elaiza
   - `_` steht für ein Zeichen:
      - `WHERE (Name LIKE 'H_se')` findet Hase, Hose, nicht aber Hause

### Spezielle Bedingungen

- **`NULL`**: `WHERE preis = NULL`
   prüfen auf nicht gesetzte Felder

- **`Date`**: `WHERE Datum <= '2000-09-31'`
   Datumswerte werden i.d.R. in einfachen Anführungszeichen nach dem Muster YYYY-MM-DD notiert

- **`Character`**: `WHERE Nachname < 'E'`
         ASCII-Code wird numerisch ausgewertet: Welcher Name fängt mit einem Buchstaben an, der in der Zeichentabelle vor dem E steht (...0-9,ABCD)
         Da der Ausdruck numerisch ausgewertet wird funktionieren auch Operationen wie `WHERE (Buchstabe BETWEEN 'M' AND 'O')`   

- **Single Row Functions** als Bedingungen (Details dazu siehe in [diesem Artikel](https://oer-informatik.de/sql-dml-singlerowfunctions)):

Der `IN`-Operator kann zur Abkürzung mehrerer `OR`-Verknüpfungen genutzt werden:
   - `WHERE Country IN ('Germany', 'France', 'UK')` ist Synonym für
   - `WHERE (Country = 'Germany') OR (Country ='France') OR (Country = 'UK');`

Die verwendbaren Operatoren weichen mitunter vom genutzten DBMS ab, Sie  finden sie in der jeweiligen SQL-Referenz. So benutzt beispielsweise MS Access % und ? als Wildcards.

## Links und weitere Informationen
- [Wikibooks-Seite zu Abweichungen der SQL-Dialekte ggü. dem SQL-ANSI-Standard](https://en.wikibooks.org/wiki/SQL_Dialects_Reference/Data_structure_definition/Data_types/Numeric_types)

