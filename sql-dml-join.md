## Daten aus mehreren Tabellen vereinen (JOIN)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/sql-dml-join</span>



> **tl/dr;** _(ca. 5 min Lesezeit): Abfragen können Daten aus mehreren Tabellen enthalten. Der Hauptweg, das zu tun, ist es, im `FROM`-Statement Tabellen mit `LEFT JOIN` oder `INNER JOIN` zu verknüpfen. Die unterschiedlichen Wege, das zu tun sowie weitere Varianten von `JOIN` werden in diesem Artikel beschrieben._


## `JOIN`: Tabellen verbinden

Während der Modellierung und Normalisierung wurden die zur Verfügung stehenden Informationen auf viele Tabellen aufgeteilt. 

Die SQL-Anweisung wird um eine Verknüpfung (gelb) und eine Bedingung (grün) erweitert:

```sql
SELECT tabelleX.spaltennameX, tabelleY.spaltennameY 
FROM tabelleX
[[LEFT] JOIN] tabelleY
[ON tabelleX.spaltennameA = tabelleY.spaltennameB]
[WHERE Bedingung = wahr]
[ORDER BY tabelle1.spaltenname1 [DESC/ASC],…]
[LIMIT Integerzahl ]
```

Erläuterung:
tabelleX.spalteY: Angezeigt wird die Spalte namens spalteY der Tabelle namens tabelleX.

LEFT JOIN / INNER JOIN in expliziter Schreibweise

In den allermeisten Fällen, in denen Daten aus zwei Tabellen verknüpft werden sollen, gilt eine der beiden Voraussetzungen, die hier kurz an einem Beispiel erläutert werden sollen:

Tabelle A		Tabelle B
Klasse	Lehrer		Schülername	Klasse	Alter
9A	Merkel		Konrad	8A	18
9B	Schröder		Ludwig	8A	16
10A	Kohl		Kurt	9A	17
10B	Schmidt		Georg	9A	21
11A	Brandt		Walter	9B	20
			Richard	10A	18
			Roman	10A	16

Es sollen alle Zeilen der Tabelle A ausgegeben werden, und nur die verknüpften Zeilen der Tabelle B.

SELECT * FROM A 
LEFT JOIN B ON A.Klasse = B.Klasse

Was fällt hierbei auf?
- sofern das Verknüpfungskriterium in einer der Tabellen nicht einzigartig (unique) ist, treten Dopplungen der zugeordneten Werte der anderen Tabelle auf (grün)
- es werden auch Zeilen der Tabelle A ausgegeben, für die bei Tabelle B mangels Zuordnung NULL-Werte notiert sind (gelb)

==> LEFT JOIN
(auch: Left outerJoin genannt)	





Klasse	Lehrer	Schülername	Klasse	Alter
9A	Merkel	Kurt	9A	17
9A	Merkel	Georg	9A	21
9B	Schröder	Walter	9B	20
10A	Kohl	Richard	10A	18
10A	Kohl	Roman	10A	16
10B	Schmidt	 	 	 
11A	Brandt	 	 	 

2. Es sollen nur Zeilen ausgegeben werden, für die sowohl Werte von Tabelle A als auch von Tabelle B existieren.


SELECT * FROM A 
INNER JOIN B ON A.Klasse = B.Klasse

Was fällt hierbei auf?
- sofern das Verknüpfungskriterium in einer der Tabellen nicht einzigartig (unique) ist, treten Dopplungen der zugeordneten Werte der anderen Tabelle auf (grün)
- Weder Tabelle A noch Tabelle B wird vollständig ausgegeben.

==> INNER JOIN
(Ergebnis entspricht einem EQUI-JOINS, siehe S.3) 	
Klasse	Lehrer	Schülername	Klasse	Alter
9A	Merkel	Kurt	9A	17
9A	Merkel	Georg	9A	21
9B	Schröder	Walter	9B	20
10A	Kohl	Richard	10A	18
10A	Kohl	Roman	10A	16


Über diese beiden Verknüpfungen hinaus gibt es eine ganze Reihe weiterer Verknüpfungen, die jedoch eher theoretische Bedeutung haben:

 	Der right (outer) join stellt einen left join mit vertauschten Tabellen dar:

SELECT * FROM A 
RIGHT JOIN B ON A.Klasse = B.Klasse



 	 	 
Bei einem full outer join werden alle Zeilen aus der Tabelle A und der Tabelle B zusammengeführt, für die Join Spaltenelemente gleiche Werte enthalten sowie alle Elemente aus den beiden Tabellen, die keine gleichen Elemente haben.

SELECT * FROM A 
LEFT JOIN B 
ON A.Klasse = B.Klasse
UNION 
SELECT * FROM A 
RIGHT JOIN B 
ON A.Klasse = B.Klasse;
	Bei dem left outer join with exclusion werden alle Zeilen aus der Tabelle A mit der Tabelle B zusammengeführt, für die es keine gleichen Elemente gibt. Es entspricht also auch dem left outer join, bei dem die Elemente des inner join entfernt wurde.

SELECT * FROM A 
LEFT OUTER JOIN B 
ON A.Klasse = B.Klasse
WHERE B.Klasse IS NULL;	Bei diesem full outer join with exclusion werden alle Zeilen der beiden Tabellen  zusammengeführt, für die es keine gleichen Elemente gibt. Er entspricht also dem full outer join, bei dem der inner join entfernt wurde.

SELECT * FROM A 
LEFT JOIN B 
ON A.Klasse = B.Klasse
WHERE B.Klasse IS NULL
UNION 
SELECT * FROM A 
RIGHT JOIN B 
ON A.Klasse = B.Klasse
WHERE A.Klasse IS NULL;
 
Implizite Schreibweisen und unübliche Verknüpfungen 
(sollte man gehört haben, in der Praxis aber sehr selten relevant)

JOINS lassen sich nicht nur über die explizite Schreibweise (JOIN), sondern auch implizit beschreiben. Hierzu werden sämtliche Tabellen im FROM-Clause kommagetrennt gelistet und die Verknüpfungsbedingungen im WHERE-Clause genannt:

Man unterscheidet die drei Verknüpfungsvarianten, wobei die ersten beiden in der Praxis unüblich sind:
1.	[unüblich] CROSS JOIN: 
In diesem Fall werden alle Kombinationsmöglichkeiten der genannten Attributwerte aus beiden Tabellen aufgestellt („kartesisches Produkt“). Da die Ergebnismenge schnell sehr groß wird und die meisten Zeilen i.d.R. nicht zur Lösung eines Problems beitragen ist der CROSS JOIN in der Praxis selten hilfreich.

Das gleiche Ergebnis erhält man, wenn man LEFT, JOIN und ON weglässt und die Tabellen mit Komma getrennt hinter FROM auflistet. 


2.	[unüblich] NATURAL JOIN: 
Aus dem kartesischen Produkt des Cross-Joins  werden automatisch diejenigen Felder verknüpft und über deren Gleichheit gefiltert, deren Feldnamen gleich lauten. Um zu vermeiden das identisch lautende Felder, die aber unterschiedliches beschreiben miteinander verknüpf werden ist der NATURAL JOIN in der Praxis unüblich.


3.	[synonym für INNER JOIN] EQUI JOIN: 
Das kartesische Produkt des Cross-Joins wird hier über WHERE –Clause gefiltert. Das besondere Merkmal des EQUI-JOINS ist, dass die Bedingung die Gleichheit (Äquivalẹnz, daher der Name) zweier Attribute sein muss (es muss also in der Bedingung das Gleichheitszeichen „=“ stehen und kein anderer Vergleichsoperator). Die Tabellen werden mit Komma getrennt hinter FROM auflistet. Die Verknüpfung der Tabellen erfolgt mit WHERE. Angezeigt werden nur die Attributwerte, die eine Verknüpfung aufweisen (entspricht somit einem INNER-JOIN in impliziter Schreibweise).

