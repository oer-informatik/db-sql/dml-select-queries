# OER-Informatik Markdown-Quelltexte

Aus allen Markdown-Dateien in diesem Ordner werden eigenständige erstellt:

*  HTML-Seiten (zu erreichen über https://oer-informatik.gitlab.io/...gruppennamen/...projektname.html )

* PDF-Dateien (zu erreichen über https://oer-informatik.gitlab.io/...gruppennamen/...projektname.pdf )

* HTML-Blöcke, die als Artikel unter [http://oer-informatik.de/...dateiname_ohne_endung...](http://oer-informatik.de) veröffentlicht werden.

Auf eine Individualisierung dieser Readme wird verzichtet.

Zur Erstellung wurde die Konfiguration der  [TIBHannover](https://gitlab.com/TIBHannover/oer/course-metadata-test/) für die CI-Pipeline von Gitlab genutzt und individualisiert. Anleitungen zur Nachnutzung finden sich im Repository der TIB, der letzte Stand meiner Anpassungen unter: [https://gitlab.com/oer-informatik/service/ci-pipeline](https://gitlab.com/oer-informatik/service/ci-pipeline)
