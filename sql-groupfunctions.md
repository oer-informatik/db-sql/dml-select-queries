## Aggregierende Funktionen

<span class="hidden-text">
https://oer-informatik.de/sql-groupfunctions
</span>

> **tl/dr;** _(ca. 5 min Lesezeit): Aggregierende Funktionen bieten die Möglichkeit, übergreifende Aussagen zu mehreren Datensätzen vorzunehmen. Gemeinsam mit dem `GROUP`-Clause wird so ermöglicht, für Datensatzgruppen beispielsweise Summen oder Durchschnitte zu bilden. Dieser Artikel reist die Grundlagen dazu an._

Aggregierende Funktionen fassen Werte aus mehreren Datensätzen (Zeilen) zusammen: es werden Summe, Extremwerte, Durchschnitte gebildet. Abgesehen von `COUNT()` lassen sie sich sinnvoll nur auf Zahlentyp-Attribute anwenden. Sie sind i.d.R. nur gemeinsam mit einem `GROUP BY`- Clause sinnvoll, wobei die Regel gilt:

**Alle im `SELECT`-Clause genannten Attribute, die nicht aggregiert werden, müssen gruppiert werden.**

Sprich: werden Attribute nicht zeilenweise über eine Aggregatsfunktion zusammengefasst, müssen Sie im `GROUP BY`-Clause genannt werden, um ausgegeben werden zu können.

Die meisten DBMS kennen die folgenden Aggregatsfunktionen für Zahlenattribute

|Name| Extremwerte  | Summe | Durchschnitt |
| --- | -------- | -------- | -------- |
|Funktionen| ```MIN()```, ```MAX()```| ```SUM()``` |```AVG(Kosten)```|
 |Beispiele|  ```MIN(Preis)```: Minimum |```SUM(Anzahl)```  |```AVG(Kosten)```  |
||  ```MAX(Preis)```: Maximum | |  |
||    | |  |

Es gibt auch einige Aggregatsfunktionen, die sich auf nicht-Zahlen-Attribute wie Zeichenketten anwenden lassen:

|Name| Erster Wert der Ergebnisliste  | Letzter Wert der Ergebnisliste | Anzahl |
|---|---| ---|---|
|Funktionen| ```FIRST()```| ```LAST()```| ```COUNT(*)``` |
 |Beispiele|  ```FIRST(name)``` |```´LAST(name)```  | ```COUNT(*)```: Zeilenanzahl <br />```COUNT(Name)```: Zeilen, in denen ```Name``` nicht NULL ist <br /> ```COUNT(DISTINCT Name)``` : Anzahl unterschiedlicher Namen|

![Railroad-Diagramm für Aggregatsfunktionen und den GROUP-BY-Clause](images/sql-group-function-railroad-detail.png)

Jedes Ausgabefeld, das in einer Abfrage mit Aggregations-Funktion nicht aggregiert wird, (durch eine Funktion) muss im GROUP-BY-Clause genannt werden.

Beispiel:

```sql
SELECT AVG(Gehalt) AS Durchschnittsgehalt, Abteilung
FROM Mitarbeiter
GROUP BY Abteilung
```

### `HAVING`-Clause und Bedingungen an Funktionsergebnisse

Da der `WHERE`-Clause vor der Berechnung der Funktionen ausgewertet wird, wird ein neues Konstrukt benötigt, um Bedingungen an Funktionsergebnisse zu knüpfen:  der `HAVING`-Clause

Syntax der erweiterten Abfrage

```sql
SELECT Funktionsname(parameter) AS aliasname, spaltennameX
FROM tabelleX
[WHERE Bedingung = wahr]
[HAVING Bedingung = wahr]
```

Darüber hinaus gibt es in den meisten DBMS (MySQL, MSSQL) auch noch einen Clause zum Sortieren (`ORDER`) und Begrenzen der Ergebnisse (`LIMIT`). Bei _Oracle_ und _postgresql_ ist die Syntax etwas anders umgesetzt. Das folgende Railroad-Diagramm gilt daher nur für MSSQL und MySQL:

![Railroad-Diagramm HAVING, ORDER, LIMIT](images/sql-group-function-railroad.png)

Die Reihenfolge der Clauses nach dem FROM ist wichtig. Als Eselsbrücke für MySQL/MariaDB kann der folgende Merksatz dienen:


| Warum | geht | Herbert | oft | Laufen |
| -------- | -------- | -------- | -------- | --------|
| WHERE [...]    | GROUP [...]     | HAVING [...]     | ORDER [...] | LIMIT [...] |

### Begrenzung der Ergebnismengen und Sortierung

Leider legen die DBMS die Eingrenzung der Ergebnismenge unterschiedlich fest:

| Funktion| MSSQL |MySQL | OracleDB | postgreSQL | SQLite|
| -------- | -------- | -------- |-------- |-------- |-------- |
| Begrenzung der Ergebnismengen | `SELECT TOP(1000)`  |LIMIT 1000| | ||

